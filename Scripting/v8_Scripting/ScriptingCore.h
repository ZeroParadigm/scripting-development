/**
 * ScriptingCore - The main core which exposes API and manages scripts
 *
 * Authors - Jesse Dillon/Jimmy Roland
 */

#pragma once

#include "../v8_Deps/v8_include/v8.h"
#include <map>
#include <vector>
#include <string>

class BaseScript;

/**
 * Associate - Struct associating actors with scripts
 */
struct Associate
{
	int actorId;
	std::string script;

	Associate(int id, std::string s) : actorId(id), script(s) {}
	Associate(const Associate&) {}

	bool operator=(const Associate& rhs)
	{
		return ((actorId == rhs.actorId) &&
				(script == rhs.script));
	}

private:
	Associate(void){}
};

/**
 * Scripting Core
 * Scripting core manages script registration, associate registartion,
 * updates, events, and shell procedures
 */
class _declspec(dllexport) ScriptingCore
{
private:

	//Map to hold currently compiled scripts
	std::map<std::string, BaseScript*> m_ScriptMap;

	//Vector of current associated scripts
	std::vector<Associate*> m_Associates;

	//v8 Isolate
	v8::Isolate* m_Isolate;

	//Global context
	v8::Persistent<v8::Context> m_GlobalContext;

public:

	ScriptingCore(void);
	~ScriptingCore(void);

	/**
	 * Initialize
	 *
	 * /create shell 
	 * Sets up the Isolate and global context
	 */
	void Initialize(bool shell);

	/** 
	 * Shutdown
	 * 
	 * Releases v8 objects and deallocates memory
	 */
	void Shutdown(void);

	/** 
	 * Update
	 * /dt - Elapsed time since last update
	 * Runs all Associated scripts
	 */
	void Update(float dt);

	/**
	 * Register continuous
	 *
	 * /file to be compiled
	 * Compiles a script and caches it in the script map (For repeated script use)
	 * Returns true if call succeeds
	 */
	bool RegisterContinuous(std::string file);

	/**
	 * SpawnEvent
	 *
	 * /file to be executed
	 * Compiles, executes, and releases a script (For single use)
	 * Returns true if call succeeds
	 */
	bool SpawnEvent(std::string file);

	/**
	 * CallFunction
	 *
	 * /string to be executed
	 * Compiles and exectues a JS command on the global object
	 * Returns true if call succeeds
	 */
	bool Inject(std::string func);

	/**
	 * Create Associate
	 *
	 * /Actor id
	 * /Script name
	 * Associates an actor with a script
	 */
	bool CreateAssociate(int id, std::string script);

	/** 
	 * Remove Associate
	 *
	 * /Actor id
	 * /Script name
	 * Removes an associate
	 */
	bool RemoveAssociate(int id, std::string script);

	/**
	 * CallScriptedFunction
	 *
	 * /Global object
	 * /function name
	 * /argument list
	 * /argument count
	 * Calls a scripted function from the global object with the provided
	 * function name and agruments
	 */
	v8::Handle<v8::Value> CallScriptedFunction(v8::Handle<v8::Object> global,
											   std::string funcName,
											   v8::Handle<v8::Value> argList[],
											   unsigned int argCount);

protected:

	/**
	 * GetIsolate
	 *
	 * Returns the v8::Isolate
	 */
	v8::Isolate* GetIsolate(void){return m_Isolate;}

	/** 
	 * AddStringToArguments
	 *
	 * /string to be added
	 * /argument list
	 * /position to enter this string
	 * Adds a string to an argument list at the given position
	 */
	void AddStringToArguments(std::string str, 
							  v8::Handle<v8::Value> argList[], 
							  unsigned int argPos);

	/**
	 * AddNumberToArguments
	 *
	 * /number to be added
	 * /argument list
	 * /position to enter number
	 * Adds a number to an argument list at the given position
	 */
	void AddNumberToArguments(double num,
							  v8::Handle<v8::Value> argList[],
							  unsigned int argPos);

	/**
	 * AddBooleanToArguments
	 *
	 * /value
	 * /argument list
	 * /position to enter bool
	 * Adds a number to an argument list at the given position
	 */
	void AddBoolToArguments(bool value,
							v8::Handle<v8::Value> argList[],
							unsigned int argPos);

private:

	/**
	 * SpinThread
	 *
	 * Spins a thread for the shell
	 */
	void SpinThread(void);

	/**
	 * Read file
	 *
	 * /name of the file to read
	 * Reads in a file and returns a handle to the resulting string
	 */
	v8::Handle<v8::String> ReadFile(const std::string& name);

	/**
	 * Clear Associates
	 *
	 * Clears all current associates
	 */

	void ClearAssociates(void);

	/**
	 * Clear Scripts
	 *
	 * Clears all active scripts
	 */
	void ClearScripts(void);

	/**
	 * Expose print method
	 */
	void ExposePrint(v8::Handle<v8::Context> context);
};

//extern "C"
//{
//	HRESULT CreateScriptObject(HINSTANCE hDLL);
//	HRESULT ReleaseScriptObject(void);
//}
//
//typedef HRESULT (*CREATESCRIPTOBJECT)(HINSTANCE hDLL);
//typedef HRESULT (*RELEASESCRIPTOBJECT)(void);