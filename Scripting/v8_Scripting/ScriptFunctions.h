/**
 * ScriptFunctions
 *
 * Defines the built-in global functions the scripting core is going to expose
 *
 * Author - Jesse Dillon/Jimmy Roland
 */

#pragma once

#include <v8.h>
#include <iostream>

void Print(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	//Print the arguments
	for (int i = 0; i < args.Length(); i++) {
		v8::HandleScope handle_scope(args.GetIsolate());
		v8::String::Utf8Value str(args[i]);
		std::string myStr = *(str);
		const char* cstr = myStr.c_str();
		printf("%s", cstr);
	}
	printf("\n", NULL);
}