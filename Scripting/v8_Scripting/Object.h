/**
 * Object - Defines a JavaScript compatible C++ object
 *
 * Author - Jesse Dillon
 */

#pragma once

#include <string>
#include <map>

/**
 * VAR_TYPE - Declares an enum for number, string, or bool
 */
enum VAR
{
	NUMBER = 0,
	STRING = 1,
	BOOL   = 2
};

/**
 * Value - Declares a union for the value
 */
union Value
{
	double  number;
	bool    boolean;
	char    string;
};

class Object
{
private:


};