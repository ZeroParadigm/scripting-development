*********README**************

This scripting library is built
on Google V8 and meant to be used
in a console or windows based application.

Authors: Jesse Dillon and Jimmy Roland (10/2013)


TO USE: Build the libs from the .sln file provided
and include them in your C++ application along with
the dependencies.